﻿using UnityEngine;
using System.Collections;

public class FlowerControllerScript : MonoBehaviour {
	public AudioClip growSound;
	public AudioSource fxSound;

	// Use this for initialization
	void Start () {
		fxSound = GetComponent<AudioSource> ();
		fxSound.PlayOneShot (growSound, 0.2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			HeroControllerScript.heroState = (int)HeroControllerScript.heroState + 1;
			Debug.Log ("Player touched the flower. Current state: "
				+ HeroControllerScript.heroState);
			Destroy (gameObject);
		}
	}

}
