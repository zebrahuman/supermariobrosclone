﻿using UnityEngine;
using System.Collections;

public class CameraFollowScript : MonoBehaviour {
	public Transform player;
	public AudioSource fxSound;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void FixedUpdate () {
		if (transform) {
			transform.position = new Vector3 (HeroControllerScript.activeObject.transform.position.x + 0.1f, 0.3f,
				-10);
			
		}
	}
}