﻿using UnityEngine;
using System.Collections;

public class MarioHeadControllerScript : MonoBehaviour {
	public AudioClip bumpSound;
	public AudioSource fxSound;

	// Use this for initialization
	void Start () {
		fxSound = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

	}


	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "question" || other.tag == "Brick") {
			Debug.Log ("Source: MarioHead. Hit: " + other.name);
			fxSound.PlayOneShot (bumpSound, 0.1f);
		}
	}
}
