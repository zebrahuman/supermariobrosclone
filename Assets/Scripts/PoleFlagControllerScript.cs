﻿using UnityEngine;
using System.Collections;

public class PoleFlagControllerScript : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FlagStartsFalling()
	{
		Debug.Log ("flag should flal now");
		GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePositionX;
		GetComponent<Rigidbody2D> ().gravityScale = 0.3f;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.layer == LayerMask.NameToLayer ("ground")) {
			GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezeAll;
			SendMessageUpwards ("DisablePoleCollider");
		}
	}
}
