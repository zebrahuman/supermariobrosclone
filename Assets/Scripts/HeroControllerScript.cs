﻿using UnityEngine;
using System.Collections;

public class HeroControllerScript : MonoBehaviour {

	public enum heroStateEnum {freshman, ateMushroom, ateFlower, invisinsible};

	public AudioClip coinConsumeSound;

	public static bool levelCompleted = false;
	public static bool lastMileStarted = false;
	public static int heroState;
	public static bool invincible = false;

	public static bool canJump = false;
	public float speed = 10f;


	public static GameObject activeObject = null;

	public Transform freshManMario;
	public Transform ateMushroomMario;
	public Transform ateFlowerMario;
	public Transform invincibleMario;

	// Use this for initialization
	void Start () {
		heroState = (int)GameState.heroStateEnum.freshman;
		activeObject = freshManMario.gameObject;
//		foreach (Transform child in transform) {
//			if (child.gameObject.activeSelf) {
//				print ("Active child found " + child.name);
//				activeObject = child.gameObject;
//			}
//		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space) && canJump) {

			activeObject.SendMessage ("Jump");
			canJump = false;
		}
	}

	void FixedUpdate() {
		float inputX;

		if (lastMileStarted && !levelCompleted) {
			inputX = 0.5f;
		} else if (lastMileStarted && levelCompleted) {
			inputX = 0.0f;
			speed = 0.0f; 
		} else {
			inputX = Input.GetAxis ("Horizontal");
		}
		activeObject.SendMessage ("Move", inputX);
	}

	void ActivateAteMushroomState(){
		ateMushroomMario.position = freshManMario.position;
		freshManMario.gameObject.SetActive (false);
		ateMushroomMario.gameObject.SetActive (true);
		activeObject = ateMushroomMario.gameObject;
	}

	void GoBackToFreshManState(){
		invincible = true;
		freshManMario.position = activeObject.transform.position;
		freshManMario.gameObject.SetActive (true);
		activeObject.SetActive (false);
		activeObject = freshManMario.gameObject;
		activeObject.layer = LayerMask.NameToLayer ("invincible");
		StartCoroutine (WaitAndDisableInvincible ());
	}

	void ActivateAteFlowerState(){
		Debug.Log ("ATE FLOWE STATE ACTIVATION NOT IMPLEMENTED YET");
//		heroState = heroState + 1;
//		freshManMario.gameObject.SetActive (false);
//		ateMushroomMario.gameObject.SetActive (true);
//		activeObject = ateMushroomMario.gameObject;
//		ateFlowerMario.position = activeObject.position;
	}

	bool IsFalling()
	{
		RaycastHit2D rc;
		print ("Active object linecasting: " + activeObject.name);
		rc = Physics2D.Linecast(activeObject.transform.position, new Vector2 (activeObject.transform.position.x,
			activeObject.transform.position.y - 0.5f),
			1 << LayerMask.NameToLayer("ground"));
		if (rc) {
			return false;
		} else {
			return true;
		}
	}


	IEnumerator WaitAndDisableInvincible() {
		yield return new WaitForSeconds(2);
		activeObject.layer = LayerMask.NameToLayer ("Mario");
	}



}
