﻿using UnityEngine;
using System.Collections;

public class OwlControllerScript : MonoBehaviour {
	public float v = 2.0f;
	Animator anim;
	public bool owlDead = false;
	public float deadOwlCollectTimeS = 0.1f;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
		
	void FixedUpdate() {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (v , GetComponent<Rigidbody2D> ().velocity.y);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player" && !owlDead) {
			Debug.Log ("Player touched the enemy. Current state: " + HeroControllerScript.heroState);
			coll.gameObject.SendMessage ("Die");
		} else if (coll.gameObject.tag == "Player" && owlDead) {
			Debug.Log ("Player touched Dead enemy. Ignoring.");
		} else if (coll.gameObject.layer == 11 /* Enemies */ || coll.gameObject.tag == "tube" ) {
			Flip ();
		}
	}

	void MarkOwlDead()
	{
		v = 0.0f;
		anim.SetBool ("OwlDead", true);
		owlDead = true;
		StartCoroutine (Waitcoroutine());

	}

	IEnumerator Waitcoroutine() {
		yield return new WaitForSeconds(deadOwlCollectTimeS);
		Destroy (gameObject);
	}

	void Flip() {
		v = v * -1;
	}
}
