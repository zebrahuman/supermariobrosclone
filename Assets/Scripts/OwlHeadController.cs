﻿using UnityEngine;
using System.Collections;

public class OwlHeadController : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player") {
			Debug.Log ("Player touched the owl head. Current state: "
				+ HeroControllerScript.heroState);
			gameObject.SendMessageUpwards ("MarkOwlDead");
		}
	}

}
