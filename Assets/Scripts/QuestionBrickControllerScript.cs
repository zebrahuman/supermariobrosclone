﻿using UnityEngine;
using System.Collections;

public class QuestionBrickControllerScript : MonoBehaviour {
	public GameObject mushroomPrefab;
	public GameObject flowerPrefab;
	public GameObject coinPrefab;
	public GameObject clone;
	public bool specialBrick;
	public bool _questionBrickHit = false;
	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "MarioHead" && !_questionBrickHit) {
			Debug.Log ("PLAYER HIT QUESTION BOTTOM");
			_questionBrickHit = true;
			anim.SetBool ("Consumed", true);
			if (HeroControllerScript.heroState == (int) GameState.heroStateEnum.freshman && 
				specialBrick) {
				SpawnMushroom ();
			} else if (HeroControllerScript.heroState == (int) GameState.heroStateEnum.ateMushroom &&
				specialBrick) {
				SpawnFlower ();
			} else if (HeroControllerScript.heroState == (int) GameState.heroStateEnum.ateFlower ||
			!specialBrick) {
				SpawnCoin ();
			}
		}
	}

	void SpawnMushroom() {
		clone =  (GameObject) Instantiate(mushroomPrefab, new Vector2(transform.position.x,
			transform.position.y + 1.0f), Quaternion.identity);
		clone.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 50f);
	}

	void SpawnFlower() {
		clone = (GameObject) Instantiate(flowerPrefab, new Vector2(transform.position.x + 0.31f,
			transform.position.y + 1.0f), Quaternion.identity);
		clone.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 50f);
	}
	void SpawnCoin() {
		clone = (GameObject) Instantiate(coinPrefab, new Vector2(transform.position.x,
			transform.position.y + 1.0f), Quaternion.identity);
		clone.GetComponent<Rigidbody2D> ().AddForce (Vector2.up * 50f);
	}
}
