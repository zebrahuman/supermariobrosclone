﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MarioControllerScript : MonoBehaviour {

	private bool facingRight = true;
	public float speed = 10f;
	public float jumpSpeed = 4300f;

	public AudioSource fxSound;

	public AudioClip jumpSmallSound;
	public AudioClip kickSound;
	public AudioClip powerUp;
	public AudioClip coinConsumeSound;

	private float tmpGravityScale;


	Animator anim;


	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		fxSound = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update () {
		
	}


	void FixedUpdate () {

	
	}

	void Move(float inputX) {
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (inputX * speed, GetComponent<Rigidbody2D> ().velocity.y);
		anim.SetFloat ("Speed", Mathf.Abs (inputX));

		if (facingRight && inputX < 0) {
			Flip ();
		} else if (!facingRight && inputX > 0) {
			Flip ();
		}
	}

	void Flip() {
		Vector2 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		facingRight = !facingRight;

	}

	void Jump() {
		anim.SetBool ("JumpInProgress", true);
		fxSound.PlayOneShot (jumpSmallSound, 0.5f);
		DoJump ();
	}

	void DoJump() {
		GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpSpeed);
	}

	void Die() {
		GameState.heroLife -= 1;
		speed = 0.0f;
		// Disable child altogether with its colliders and etc
		Transform ch = transform.GetChild (0);
		ch.gameObject.SetActive (false);

		// Disable our own colliders
		GetComponent<PolygonCollider2D>().enabled = false;
		GetComponent<BoxCollider2D>().enabled = false;

		anim.SetBool ("Dead",true);

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D>().velocity.x, 20.0f);

		Debug.Log ("Remaining life:"  + GameState.heroLife);
		StartCoroutine (WaitAndReloadScene ());
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.layer == 10) {  // Ground layer
			Debug.Log ("Trigger entered with ground:" + other.name);
			anim.SetBool ("JumpInProgress", false);
			if (anim.GetBool ("OnPole")) {
				anim.SetBool ("OnPole", false);
				GetComponent<Rigidbody2D> ().gravityScale = tmpGravityScale;
			}
			HeroControllerScript.canJump = true;
		}
		if (HeroControllerScript.lastMileStarted && other.gameObject.tag == "CastleDoor") {
			Debug.Log ("Castledoor reached");
			//speed = 0.0f;
			//anim.SetFloat ("Speed", 0.0f);
			HeroControllerScript.levelCompleted = true;
		}
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "coin") {
			Debug.Log ("Mario touched the coin. Current state: " + HeroControllerScript.heroState);
			GameState.coinScore += GameState.coinWorth;
			fxSound.PlayOneShot (coinConsumeSound);
			Destroy (coll.gameObject);
		} else if (coll.gameObject.tag == "EnemyHead") {
			fxSound.PlayOneShot (kickSound);
		} else if (coll.gameObject.tag == "Pole") {
			anim.SetBool ("OnPole", true);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0.0f, 0.0f);
			tmpGravityScale = GetComponent<Rigidbody2D> ().gravityScale;
			GetComponent<Rigidbody2D> ().gravityScale = 0.5f;
		} else if (coll.gameObject.tag == "edible") {
			HeroControllerScript.heroState = HeroControllerScript.heroState + 1;
			Debug.Log ("Player touched the mushroom. Current state: "
				+ HeroControllerScript.heroState);
			Destroy (coll.gameObject);
			fxSound.PlayOneShot (powerUp);
			SendMessageUpwards ("ActivateAteMushroomState");
			gameObject.SetActive (false);
		
		}
	}



	void LastMileToCastle() {
		HeroControllerScript.lastMileStarted = true;
	}

	IEnumerator WaitAndReloadScene() {
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
