﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeControllerScript : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		GetComponent<Text> ().text = ((int) GameState.timeLeft).ToString();
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<Text> ().text = ((int) GameState.timeLeft).ToString();
		GameState.timeLeft = GameState.timeLeft - Time.deltaTime;
	}
}
