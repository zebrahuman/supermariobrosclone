﻿using UnityEngine;
using System.Collections;

public class MarioLegControllerScript : MonoBehaviour {
	public float jumpSpeed = 4300f;
	public bool canJump = false;

	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		// Jump
		if (Input.GetKeyDown(KeyCode.Space) && canJump) {
			anim.SetBool("JumpInProgress",true);
			Jump ();
		}

	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "floor") {
			anim.SetBool ("JumpInProgress", false);
			canJump = true;
		}
	}

	void Jump() {
		GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpSpeed);
		canJump = false;
	}
}
