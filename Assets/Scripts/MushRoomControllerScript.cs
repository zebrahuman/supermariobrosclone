﻿using UnityEngine;
using System.Collections;

public class MushRoomControllerScript : MonoBehaviour {
	public AudioClip growSound;
	public AudioSource fxSound;
	// Use this for initialization
	void Start () {
		fxSound = GetComponent<AudioSource> ();
		fxSound.PlayOneShot (growSound, 0.2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
//		if (coll.gameObject.tag == "Player") {
//			MarioControllerScript.heroState = (int)MarioControllerScript.heroState + 1;
//			Debug.Log ("Player touched the mushroom. Current state: "
//				+ MarioControllerScript.heroState);
//			Destroy (gameObject);
//		} else
		if (coll.gameObject.tag == "question") {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (3f, 0);
		} else if (coll.gameObject.tag == "tube") {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-3f, 0);
		}
	}
}
