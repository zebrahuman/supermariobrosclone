﻿using UnityEngine;
using System.Collections;



public class PoleControllerScript : MonoBehaviour {
	public bool flagDown = false;
	public Transform player;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Player"  && !flagDown) {
			BroadcastMessage ("FlagStartsFalling");
		}
	}
	void DisablePoleCollider() {
		flagDown = true;
		Debug.Log ("Flag is down");
		GetComponent<BoxCollider2D> ().enabled = false;
		player.SendMessage ("LastMileToCastle");
	}

}
